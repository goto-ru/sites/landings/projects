from bs4 import BeautifulSoup
import csv
import re
data = open('oldindex.html').read()
soup = BeautifulSoup(data, 'html.parser')
writer = csv.writer(open('exported.csv', 'w'))
for project in soup.find_all(class_='project_box_content'):
    title = project.find('h3').text
    text= project.find('p')
    if text:
        text = text.text
        text = re.sub("\s+", " ",  text)
        text = re.sub("^\s+","", text)

        raw_tags = project.find_all(class_='project_box_tag')
        tags = [tag.text[1:] for tag in raw_tags]
        print(tags)
        writer.writerow([title, text, ",".join(tags)])
