import csv
import os
from jinja2 import Template

projects = []
curators = []

with open("projects.csv", newline="") as csvfile:
    reader = csv.reader(csvfile)
    next(reader)
    for row in reader:
        tags = [tag.strip() for tag in row[2].split(",")]
        print(tags)
        projects.append({"name": row[0], "description": row[1], "tags": tags})

with open("curators.csv", newline="") as csvfile:
    reader = csv.reader(csvfile)
    next(reader)
    for row in reader:
        tags = [tag.strip() for tag in row[3].split(",") if len(tag) > 1]
        print(tags)
        curators.append(
            {"name": row[0], "image": row[1], "description": row[2], "tags": tags}
        )

project_tags = set([tag for project in projects for tag in project["tags"]])

curators = [curator for curator in curators if set(curator["tags"]) & project_tags]

raw = open("template.html", "r").read()
template = Template(raw)
result = template.render(projects=projects, curators=curators)
f = open("index.html", "w")
f.write(result)
